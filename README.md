#OOP17-battleship

The aim of this project is create a digital version of the classical game "battleship".

##System requirmnent

Java 1.8 or above.

Note: if you run this jar on linux with OpenJDK you should also install OpenJFX, all our test have been run on a Java Oracle environment.

##Installing

Just run the .jar file in the download section.

On linux system you may need to run it via command line `java -jar battleship.jar`.

##Run the demo

Download the provided file ( `player.db` and  `stats.db`)from the download section and when the application requires it, provide those file as input.

##Used librarie

* JavaFX
* [Junit 4](https://junit.org/junit4/)
* [Log4J 2](https://logging.apache.org/log4j/2.x/)
* [Apache commons](https://commons.apache.org/)
* [Lombok 1.6](https://projectlombok.org/)

##Contacts
* Montanari Sofia <sofia.montanari2@studio.unibo.it>
* Montelli Francesco <francesco.montelli@studio.unibo.it>
* Morini Sara <sara.morini4@studio.unibo.it> 
